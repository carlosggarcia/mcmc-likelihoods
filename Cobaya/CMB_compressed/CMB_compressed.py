import numpy as np
from cobaya.likelihood import Likelihood
import os

class CMB_compressed_logp(Likelihood):
    data_file: str  # Path to the .npz file

    def initialize(self):
        # Load the mean and covariance from the .npz file
        data = np.load(self.data_file)
        self.mean = data['mean']
        self.covariance = data['cov']

        # Compute the inverse of the covariance matrix
        self.inv_cov = np.linalg.inv(self.covariance)


    def get_requirements(self):
        return {'n_s', 'ra_rec', 'rs_rec', 'CLASS_background'}

    def calculate(self, state, want_derived=True, **params_values_dict):
        Omega_m = self.provider.get_param('Omega_m')
        ra_rec = self.provider.get_param('ra_rec')  # = D_A
        rs_rec = self.provider.get_param('rs_rec')

        b = self.provider.get_CLASS_background()
        H0 = b['H [1/Mpc]'][-1]
        Omega_b = b['(.)rho_b'][-1]/b['(.)rho_crit'][-1]

        R = np.sqrt(Omega_m) * H0 * ra_rec
        l_a = np.pi * ra_rec / rs_rec
        omega_b = Omega_b * self.provider.get_param('h')**2
        n_s = self.provider.get_param('n_s')

        mean_step = np.array([R, l_a, omega_b, n_s])
        # Compute the difference between the observed and predicted mean
        delta = self.mean - mean_step

        # Compute the log-likelihood
        chi2 = delta.dot(self.inv_cov).dot(delta)
        state['logp'] = -0.5 * chi2


# if __name__ == '__main__':
#     # Generate P18 TTTEEElowE wLCDM compressed data from
#     # https://arxiv.org/pdf/1808.05724
#     fname = 'Planck18_TTTEEElowE_compressed'
#
#     if not os.path.isfile(fname):
#         # R, la, omega_b, n_s
#         corr = np.array([[1., 0.47, -0.66, -0.71],
#                          [0.47, 1., -0.34, -0.36],
#                          [-0.66, -0.34, 1., 0.44],
#                          [-0.72, -0.36, 0.44, 1.]])
#         mean = np.array([1.7493, 301.462, 0.02239, 0.9653])
#         sigma = np.array([0.0047, 0.090, 0.00015, 0.0044])
#
#         cov = corr * sigma[None, :] * sigma[:, None]
#
#         np.savez_compressed(fname, mean=mean, cov=cov,
#                             order=['R', 'l_a', 'omega_b', 'n_s'])
