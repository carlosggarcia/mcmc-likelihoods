"""
Generate a mock SNe compressed data set for a LCDM fiducial cosmology
"""

import numpy as np
from classy import Class
from matplotlib import pyplot as plt

# File names
fname = './Planck18_TTTEEElowE_compressed.npz'
fname_out = fname.replace('.npz', '_mockdata_lcdm.npz')

# Data
data = np.load(fname)
mean = data['mean']
covariance = data['cov']
order = data['order']

# Cosmology (default 'Omega_m': 0.30988304304812053, 'h': 0.6781)
cosmo = Class()
cosmo.compute()

# Mock data
derived = cosmo.get_current_derived_parameters(['Omega_m', 'ra_rec', 'rs_rec'])
Omega_m = derived['Omega_m']
ra_rec = derived['ra_rec']
rs_rec = derived['rs_rec']
H0 = cosmo.Hubble(0)

R = np.sqrt(Omega_m) * H0 * ra_rec
l_a = np.pi * ra_rec / rs_rec
omega_b = cosmo.omega_b()
n_s = cosmo.n_s()

mean_lcdm = np.array([R, l_a, omega_b, n_s])

print(mean)
print(mean_lcdm)

# Free resources
cosmo.struct_cleanup()

# Keep the relative errors
correction = mean_lcdm / mean
covariance_lcdm = covariance * (correction[:, None] * correction[None, :])

print(covariance)
print(covariance_lcdm)

print(np.sqrt(np.diag(covariance)))
print(np.sqrt(np.diag(covariance_lcdm)))


np.savez_compressed(fname_out, mean=mean_lcdm, cov=covariance_lcdm,
                    order=order)
