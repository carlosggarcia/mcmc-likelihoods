import numpy as np
from cobaya.likelihood import Likelihood

class SNe_Einv_compressed(Likelihood):
    data_file: str  # Path to the .npz file

    def initialize(self):
        # Load the mean and covariance from the .npz file
        data = np.load(self.data_file)
        self.mean = data['mean']
        self.covariance = data['cov']
        self.z_knots = data['z_knots']

        # Compute the inverse of the covariance matrix
        self.inv_cov = np.linalg.inv(self.covariance)

    def get_requirements(self):
        # Require the Hubble parameter H(z) at the specified redshifts
        z_arr = np.concatenate([[0], self.z_knots])
        return {'Hubble': {'z': z_arr}}

    def calculate(self, state, want_derived=True, **params_values_dict):
        # Get the H(z) from the parameters
        H0 = self.provider.get_Hubble(z=0)
        Ez = self.provider.get_Hubble(z=self.z_knots) / H0

        # Compute the difference between the observed and predicted mean
        delta = 1/Ez - self.mean

        # Compute the log-likelihood
        chi2 = delta.dot(self.inv_cov).dot(delta)
        state['logp'] = -0.5 * chi2

