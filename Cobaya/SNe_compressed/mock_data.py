"""
Generate a mock SNe compressed data set for a LCDM fiducial cosmology
"""

import numpy as np
from classy import Class
from matplotlib import pyplot as plt

# File names
# fname = './PantheonPlus_Einv_compressed.npz'
fname = './DESY5_Einv_compressed.npz'
fname_out = fname.replace('.npz', '_mockdata_lcdm.npz')

# Data
data = np.load(fname)
mean = data['mean']
covariance = data['cov']
z_knots = data['z_knots']

# Cosmology (default 'Omega_m': 0.30988304304812053, 'h': 0.6781)
cosmo = Class()
cosmo.compute()

# Mock data (mean = 1/E(z) = H0 / H(z))
H0 = cosmo.Hubble(z=0)
mean_lcdm = H0 / np.array([cosmo.Hubble(z=zi) for zi in z_knots])

# Free resources
cosmo.struct_cleanup()

# Keep the relative errors
correction = mean_lcdm / mean
covariance_lcdm = covariance * (correction[:, None] * correction[None, :])



plt.errorbar(z_knots, mean, yerr=np.sqrt(np.diag(covariance)), fmt='.',
             label='Data')
plt.errorbar(z_knots + 0.01, mean_lcdm, yerr=np.sqrt(np.diag(covariance_lcdm)),
             fmt='.', label='Mock data')
plt.xlabel('$z$')
plt.ylabel('$1/E(z)$')
plt.title(fname)
plt.legend()
plt.show()
plt.close()


np.savez_compressed(fname_out, mean=mean_lcdm, cov=covariance_lcdm,
                    z_knots=z_knots)
