import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import quad
from cobaya.theory import Theory

class SplineEinv(Theory):
    z_knots: list = []

    def initialize(self):
        return

    def calculate(self, state, want_derived=True, **params_values_dict):
        # d_a = c / H0 *  int(dz / (E(z))) / (1 + z)
        E_inv_knots = params_values_dict.get('E_inv_knots')

        z_knots = np.concatenate([[0], self.z_knots])
        E_inv_knots = np.concatenate([[1], E_inv_knots])

        E_inv = interp1d(z_knots, E_inv_knots, kind='cubic',
                         fill_value="extrapolate")


        da_arr = np.zeros_like(self.z_output)
        z_arr = np.concatenate([[0], self.z_output])
        for i in range(da_arr.size):
            z0 = z_arr[i]
            zi = z_arr[i+1]
            da_arr[i] =  quad(E_inv, z0, zi)[0]
        da_arr = np.cumsum(da_arr) / (1 + self.z_output)

        c = 299792.458  # km/s
        H0 = params_values_dict['H0']  # km/s/Mpc

        da_arr *= c / H0
        state["angular_diameter_distance"] = da_arr

        # from matplotlib import pyplot as plt
        # import pyccl as ccl
        # cosmo = ccl.Cosmology(Omega_c=0.27, Omega_b=0.045, h=0.67, A_s=2.1e-9, n_s=0.96)
        # da_arr_ccl = cosmo.angular_diameter_distance(1/(1+self.z_output))
        # plt.plot(self.z_output, da_arr/da_arr_ccl)
        # plt.show()
        # dkjd

    def get_requirements(self):
        return ['E_inv_knots', 'H0']

    def must_provide(self, **requirements):
        if 'angular_diameter_distance' not in requirements:
            return {}

        self.z_output = requirements['angular_diameter_distance']['z']

        return {}

    def get_angular_diameter_distance(self, z):
        assert np.all(z == self.z_output)
        return self._current_state["angular_diameter_distance"]
