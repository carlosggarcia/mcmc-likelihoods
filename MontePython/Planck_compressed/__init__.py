from montepython.likelihood_class import Likelihood
import numpy as np


class Planck_compressed(Likelihood):

    # initialisation of the class is done within the parent Likelihood_prior. For
    # this case, it does not differ, actually, from the __init__ method in
    # Likelihood class.

    def __init__(self, path, data, command_line):

        Likelihood.__init__(self, path, data, command_line)
        # Covariance matrix_ij = sigma_i sigma_j Correlation_matrix_ij
        self.sigmas = np.array(self.sigmas).reshape((-1, 1))  # Column vector
        self.means = np.array(self.means)

        Cov = self.Corr * self.sigmas.dot(self.sigmas.T)
        print(Cov)
        print(self.sigmas)
        print(self.sigmas.dot(self.sigmas.T))
        self.iCov = np.linalg.inv(Cov)


    def loglkl(self, cosmo, data):

        derived_params = cosmo.get_current_derived_parameters(['rs_rec', 'ra_rec'])

        DAcm_star = derived_params['ra_rec'] # Comoving angular diameter distance
        rs_star = derived_params['rs_rec']

        R = np.sqrt(cosmo.Omega_m()) * cosmo.Hubble(0) * DAcm_star
        l_a = np.pi * DAcm_star / rs_star
        omega_b = cosmo.omega_b()
        n_s = cosmo.n_s()

        # Using self in order to be accesible by data_mp_likelihoods.py
        # work-in-class module.
        # https://github.com/miguelzuma/work-in-class &&
        # https://github.com/ardok-m/work-in-class &&
        self.values = np.array([R, l_a, omega_b, n_s])

        # loglkl = -1/2 * (x_i - x_mean_i) * 1/sigma_i * corr_mat_ij^-1 *
        #                 1/sigma_j * (x_j - x_mean_j)

        diff = self.values - self.means

        loglkl = -0.5 * diff.dot(self.iCov.dot(diff))

        return loglkl
