# MCMC likelihoods

Suite of likelihoods for MCMC's. Mainly for MontePython and CLASS/hi_class.

Likelihoods:
- Planck_compressed: CMB distance priors from Planck 2018 (arXiv: 1808.05724)
