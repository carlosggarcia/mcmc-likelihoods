#!/usr/bin/python
from montepython.likelihood_class import Likelihood_prior
import numpy as np

class SolarSystem_galileon(Likelihood_prior):

    def loglkl(self, cosmo, data):
        derived = cosmo.get_current_derived_parameters(['parameters_smg_real__4', 'parameters_smg_real__5'])
        c3 = derived['parameters_smg_real__4']
        beta = derived['parameters_smg_real__5']
        print(c3)
        print(beta)
        print(np.abs(c3)/beta**3)

        if np.abs(c3)/beta**3 > self.llr_limit:
           loglkl = 0.0
        else:
           loglkl = data.boundary_loglike

        return loglkl
