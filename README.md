# MCMC likelihoods

Suite of likelihoods and modules for MCMC samplers. Mainly for MontePython and CLASS/hi_class.

Likelihoods:
- Planck_compressed: CMB distance priors from Planck 2018 (arXiv: 1808.05724)
- Solar System constraints for galileon (2003.06396 eq. 54)

Modules:
- Cobaya theory module to compute the angular diameter distance given $1/E(z)$
    at different redshifts (based on 1710.00844)
